<?php

declare(strict_types=1);

namespace Smtm\Battleships;

/**
 * Class ConfigProvider
 *
 * @package Smtm\Battleships
 */
class ConfigProvider
{
    /**
     * @return array
     */
    public function __invoke(): array
    {
        return [
            'routes'       => $this->getRoutes(),
            'templates'    => $this->getTemplates(),
            'dependencies' => $this->getDependencies(),
            'battleships'  => $this->getBattleships(),
        ];
    }

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        return [
            'home'         => [
                'path'   => '/',
                'method' => 'get',
                'action' => [
                    'controller' => \Smtm\Battleships\Controller\HomeController::class,
                    'method'     => 'home',
                ],
            ],
            'battle.get'   => [
                'path'   => '/battle',
                'method' => 'get',
                'action' => [
                    'controller' => \Smtm\Battleships\Controller\BattleController::class,
                    'method'     => 'read',
                ],
            ],
            'battle.shoot' => [
                'path'   => '/battle/shoot',
                'method' => 'post',
                'action' => [
                    'controller' => \Smtm\Battleships\Controller\BattleController::class,
                    'method'     => 'shoot',
                ],
            ],
            'battle.cheat' => [
                'path'   => '/battle/cheat',
                'method' => 'get',
                'action' => [
                    'controller' => \Smtm\Battleships\Controller\BattleController::class,
                    'method'     => 'cheat',
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function getTemplates(): array
    {
        return [
            'layout'  => __DIR__ . '/../templates/layout.phtml',
            'title'   => 'Battleships (by Milen Kirilov) (PHP version)',
            'head'    => [
                'battlefield-pre-head'         => __DIR__ . '/../templates/battle/battle-pre-head.phtml',
                'battlefield-in-progress-head' => __DIR__ . '/../templates/battle/battle-in-progress-head.phtml',
            ],
            'content' => [
                '404'                     => __DIR__ . '/../templates/error/404.phtml',
                'battlefield-pre'         => __DIR__ . '/../templates/battle/battle-pre.phtml',
                'battlefield-in-progress' => __DIR__ . '/../templates/battle/battle-in-progress.phtml',
            ],
        ];
    }

    /**
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            \Smtm\Battleships\Controller\ErrorController::class  => \Smtm\Battleships\Controller\Factory\ErrorControllerFactory::class,
            \Smtm\Battleships\Controller\HomeController::class   => \Smtm\Battleships\Controller\Factory\HomeControllerFactory::class,
            \Smtm\Battleships\Controller\BattleController::class => \Smtm\Battleships\Controller\Factory\BattleControllerFactory::class,
            \Smtm\Battleships\Model\Battle::class                => \Smtm\Battleships\Model\Factory\BattleFactory::class,
        ];
    }

    /**
     * @return array
     */
    public function getBattleships()
    {
        return [
            'x'           => 10,
            'y'           => 10,
            'ships'       => [
                4 => 2,
                5 => 1,
            ],
            'positioning' => [
                'rotationAngles' => [
                    0,
                    11,
                    22,
                    34,
                    45,
                    56,
                    68,
                    79,
                    90,
                    101,
                    112,
                    124,
                    135,
                    146,
                    158,
                    169,
                    180,
                    191,
                    202,
                    214,
                    225,
                    236,
                    248,
                    259,
                    270,
                    281,
                    292,
                    304,
                    315,
                    326,
                    338,
                    349
                ], // some optimization wouldn't hurt anyone :)
            ],
        ];
    }
}
