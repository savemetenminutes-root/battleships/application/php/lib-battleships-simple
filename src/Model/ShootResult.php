<?php

namespace Smtm\Battleships\Model;

/**
 * Class ShootResult
 *
 * @package Smtm\Battleships\Model
 */
class ShootResult
{
    const OUTCOME_FAILURE_GAME_NOT_IN_PROGRESS = 'outcome_failure_game_not_in_progress';
    const OUTCOME_FAILURE_INVALID_COORDINATES = 'outcome_failure_invalid_coordinates';
    const OUTCOME_FAILURE_ALREADY_SHOT_AT = 'outcome_failure_already_shot_at';

    const OUTCOME_SUCCESS_MISS = 'outcome_success_miss';
    const OUTCOME_SUCCESS_HIT = 'outcome_success_hit';

    /**
     * @var string|null
     */
    public $outcome;
    /**
     * @var bool
     */
    public $shipSunken;
    /**
     * @var bool
     */
    public $victory;
    /**
     * @var int
     */
    public $round;

    /**
     * ShootResult constructor.
     *
     * @param string|null $outcome
     * @param bool $shipSunken
     * @param bool $victory
     * @param int $round
     */
    public function __construct(?string $outcome = null, bool $shipSunken = false, bool $victory = false, int $round = 0)
    {
        $this->outcome    = $outcome;
        $this->shipSunken = $shipSunken;
        $this->victory    = $victory;
        $this->round      = $round;
    }
}
