<?php

namespace Smtm\Battleships\Model;

/**
 * Class Position
 *
 * @package Smtm\Battleships\Model
 */
class Position
{
    /**
     * @var int
     */
    public $size;
    /**
     * @var int
     */
    public $index;
    /**
     * @var int
     */
    public $x;
    /**
     * @var int
     */
    public $y;
    /**
     * @var int
     */
    public $deg;
    /**
     * @var array
     */
    public $coordinates;
    /**
     * @var float
     */
    public $perspective;
    /**
     * @var float
     */
    public $perspectiveLenX;
    /**
     * @var float
     */
    public $perspectiveLenY;


    /**
     * Position constructor.
     *
     * @param int $size
     * @param int $index
     * @param int $x
     * @param int $y
     * @param int $deg
     * @param array $coordinates
     * @param float $perspective
     * @param float $perspectiveLenX
     * @param float $perspectiveLenY
     */
    public function __construct(
        int $size,
        int $index,
        int $x,
        int $y,
        int $deg,
        array $coordinates,
        float $perspective,
        float $perspectiveLenX,
        float $perspectiveLenY
    ) {
        $this->size            = $size;
        $this->index           = $index;
        $this->x               = $x;
        $this->y               = $y;
        $this->deg             = $deg;
        $this->coordinates     = $coordinates;
        $this->perspective     = $perspective;
        $this->perspectiveLenX = $perspectiveLenX;
        $this->perspectiveLenY = $perspectiveLenY;
    }
}
