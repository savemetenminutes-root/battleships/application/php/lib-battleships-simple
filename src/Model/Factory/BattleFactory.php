<?php

namespace Smtm\Battleships\Model\Factory;

use Smtm\Battleships\Model\Battle;

/**
 * Class BattleFactory
 *
 * @package Smtm\Battleships\Model\Factory
 */
class BattleFactory
{
    /**
     * @param array $config
     * @param $request
     * @return mixed
     */
    public function __invoke(array $config, $request)
    {
        $_SESSION['smtm']['battleships']['battle'] = $_SESSION['smtm']['battleships']['battle'] ?? null;
        if (
            $_SESSION['smtm']['battleships']['battle'] === null
            && array_key_exists('inputGridX', $request['request'])
            && array_key_exists('inputGridY', $request['request'])
            && array_key_exists('inputShips', $request['request'])
            && is_array($request['request']['inputShips'])
            && !empty(
            array_reduce(
                $request['request']['inputShips'],
                function ($carry, $shipSize) {
                    return $carry + $shipSize;
                },
                0
            )
            )
        ) {
            $battleshipsConfig                         = [
                'x'     => $request['request']['inputGridX'],
                'y'     => $request['request']['inputGridY'],
                'ships' => array_filter($request['request']['inputShips']),
            ];
            $battleshipsConfig                         = array_replace($config['battleships'], $battleshipsConfig);
            $_SESSION['smtm']['battleships']['battle'] = $battle = new Battle($battleshipsConfig);
            $_SESSION['smtm']['battleships']['battle']->restart();
        }

        return $_SESSION['smtm']['battleships']['battle'];
    }
}
