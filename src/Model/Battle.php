<?php

namespace Smtm\Battleships\Model;

/**
 * Class Battle
 *
 * @package Smtm\Battleships\Model
 */
class Battle implements \JsonSerializable
{
    const STATUS_PRE_GAME    = 'status_pre_game';
    const STATUS_IN_PROGRESS = 'status_in_progress';
    const STATUS_GAME_WON = 'status_game_won';

    /**
     * @var array
     */
    protected $config;
    /**
     * @var array
     */
    protected $sizes;
    /**
     * @var array
     */
    protected $positions;
    /**
     * @var array
     */
    protected $filterPositions;
    /**
     * @var array
     */
    protected $finalPositions;
    /**
     * @var string
     */
    protected $status      = self::STATUS_PRE_GAME;
    /**
     * @var int
     */
    protected $round       = 0;
    /**
     * @var array
     */
    protected $shotsTaken  = [];
    /**
     * @var array
     */
    protected $targetsLeft = [];

    /**
     * Battle constructor.
     *
     * @param $config
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return [
            'config' => $this->config,
            'sizes'  => $this->sizes,
            'status' => $this->status,
            'round'  => $this->round,
        ];
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function restart()
    {
        $this->place();
        $this->targetsLeft = $this->finalPositions;
        $this->status      = self::STATUS_IN_PROGRESS;
    }

    /**
     *  Places a number of ships on the battlefield at random
     *
     * @return void
     * @throws \Exception
     */
    public function place()
    {
        $this->round           = 0;
        $this->filterPositions = $this->positions = $this->calculatePositions($this->config);

        $this->finalPositions = [];
        $shipsLeft            = 0;

        $this->sizes = $sizes =
            array_map(
                function ($element) {
                    return range(0, $element - 1);
                },
                $this->config['ships']
            );

        do {
            $randomStartingPosition = $this->getRandomPosition($this->config, $sizes, $this->filterPositions);
            array_push($this->finalPositions, $randomStartingPosition);
            foreach ($randomStartingPosition->coordinates as $element) {
                $dx = $element['x'];
                $dy = $element['y'];
                // TODO
                //document.getElementById('cell-' + dx + '-' + dy).className = document.getElementById('cell-' + dx + '-' + dy).className.replace(new RegExp('\\s*ship' + $this->size + $this->index), '') + ' ship' + $this->size + $this->index;
            }

            foreach ($this->filterPositions as $filterSize => $filterSizes) {
                foreach ($filterSizes as $filterIndex => $filterPositions) {
                    foreach ($filterPositions as $filterPositionIndex => $filterPosition) {
                        if (
                            $filterSize === $randomStartingPosition->size
                            && $filterIndex === $randomStartingPosition->index
                            && $filterPosition->coordinates[0]['x'] === $randomStartingPosition->coordinates[0]['x']
                            && $filterPosition->coordinates[0]['y'] === $randomStartingPosition->coordinates[0]['y']
                            && $filterPosition->deg === $randomStartingPosition->deg
                        ) {
                            continue;
                        }

                        $collision = false;
                        $filterDeg = $filterPosition->deg;
                        foreach ($filterPosition->coordinates as $filterCoordinateIndex => $filterCoordinate) {
                            $filterX = $filterCoordinate['x'];
                            $filterY = $filterCoordinate['y'];
                            foreach ($randomStartingPosition->coordinates as $rsspCoordinate) {
                                if (
                                    $rsspCoordinate['x'] === $filterX
                                    && $rsspCoordinate['y'] === $filterY
                                ) {
                                    $collision = true;
                                    break;
                                }
                            }

                            if ($collision) {
                                // here we delete the colliding position from the positionsIndex and filterPositions collections
                                //$this->filterPositions[filterSize][filterIndex].splice(filterPositionIndex, 1);
                                unset($this->filterPositions[$filterSize][$filterIndex][$filterPositionIndex]);
                                break;
                            }
                        }
                    }
                }
            }

            // Here we shift out the processed ship size/index so we can move to the next one
            unset($sizes[$randomStartingPosition->size][$randomStartingPosition->index]);
            if (!count($sizes[$randomStartingPosition->size])) {
                unset($sizes[$randomStartingPosition->size]);
            }

            $shipsLeft =
                array_reduce(
                    $sizes,
                    function ($carry, $element) {
                        return $carry + count($element);
                    },
                    0
                );
        } while ($shipsLeft > 0);
    }

    /**
     * Calculates and returns an array of all possible ship positions for all ships
     *
     * @return array
     */
    public function calculatePositions()
    {
        $positions = [];
        foreach ($this->config['ships'] as $size => $indexes) {
            $positions[$size] = [];
            for ($index = 0; $index < $indexes; $index++) {
                $positions[$size][$index] = [];
                for ($x = 1; $x <= $this->config['x']; $x++) {
                    for ($y = 1; $y <= $this->config['y']; $y++) {
                        foreach ($this->config['positioning']['rotationAngles'] as $deg) {
                            $inBounds    = 1;
                            $coordinates = [];
                            // This next constant improves the accuracy of a pivoted line on a cell/pixel grid: The closer it gets to 45 degree rotation the longer the hypotenuse is (len * lenStep)
                            $perspective     = ($size + ((sqrt(2) * $size - $size) * (45 - abs($deg % 90 - 45))) / 45) / $size;
                            $perspectiveLenX = $size * $perspective * cos($deg * pi() / 180);
                            $perspectiveLenY = $size * $perspective * sin($deg * pi() / 180);
                            for ($len = 0; $len < $size; $len++) {
                                $ddx = $len * $perspective * cos($deg * pi() / 180);
                                $ddy = $len * $perspective * sin($deg * pi() / 180);
                                $dx  = $x + (int)round($ddx);
                                $dy  = $y + (int)round($ddy);
                                if ($dx > 0 && $dx <= $this->config['x'] && $dy > 0 && $dy <= $this->config['y']) {
                                    array_push(
                                        $coordinates,
                                        [
                                            'x'   => $dx,
                                            'y'   => $dy,
                                            'len' => $len,
                                            'deg' => $deg,
                                            'ddx' => $ddx,
                                            'ddy' => $ddy,
                                        ]
                                    );
                                } else {
                                    $inBounds = 0;
                                    break;
                                }
                            }
                            // We use the equation of the lines to calculate whether two ships overlap
                            $diffX = ($coordinates[0]['x'] - $coordinates[count($coordinates) - 1]['x']);
                            $slope = 0;
                            if ((float)$diffX !== 0.0) { // whether the slope is not infinite (vertical line)
                                $slope = ($coordinates[0]['y'] - $coordinates[count($coordinates) - 1]['y']) / $diffX;
                            }
                            $intercept = $coordinates[0]['y'] - $slope * $coordinates[0]['x'];
                            if ($inBounds) {
                                array_push(
                                    $positions[$size][$index],
                                    new Position($size, $index, $x, $y, $deg, $coordinates, $perspective,
                                        $perspectiveLenX, $perspectiveLenY, $slope, $intercept)
                                );
                            }
                        }
                    }
                }
            }
        }

        return $positions;
    }

    /**
     * Picks a random ship position
     *
     * @param $config
     * @param $sizes
     * @param $filterPositions
     * @return Position
     * @throws \Exception
     */
    public function getRandomPosition($config, $sizes, $filterPositions): Position
    {
        $size          = array_rand($sizes);
        $index         = array_rand($sizes[$size]);
        $positionIndex = array_rand($filterPositions[$size][$index]);
        $position      = $filterPositions[$size][$index][$positionIndex];

        return $position;
    }

    /**
     * @param $x
     * @param $y
     * @return ShootResult
     */
    public function shoot($x, $y): ShootResult
    {
        $result = new ShootResult();

        if ($this->status !== self::STATUS_IN_PROGRESS) {
            $result->outcome = ShootResult::OUTCOME_FAILURE_GAME_NOT_IN_PROGRESS;
            return $result;
        }

        if (!is_numeric($x) || $x < 1 || $x > $this->config['x'] || !is_numeric($y) || $y < 1 || $y > $this->config['y']) {
            //$this->logger.logError('Invalid coordinates.');
            $result->outcome = ShootResult::OUTCOME_FAILURE_INVALID_COORDINATES;
            return $result;
        }

        if (
        !empty(
        array_reduce(
            $this->shotsTaken,
            function ($carry, $coordinates) use ($x, $y) {
                if ($coordinates['x'] === $x && $coordinates['y'] === $y) {
                    array_push($carry, $coordinates);
                }
                return $carry;
            },
            []
        )
        )
        ) {
            //$this->logger.logError(`You have already fired a shot @ coordinates ${x}x${y}.`);
            $result->outcome = ShootResult::OUTCOME_FAILURE_ALREADY_SHOT_AT;
            return $result;
        }

        $this->round++;
        $result->round = $this->round;
        array_push($this->shotsTaken, ['x' => $x, 'y' => $y]);

        if (
        !empty(
        array_reduce(
            $this->finalPositions,
            function ($carry, $position) use ($x, $y) {
                return
                    array_merge(
                        $carry,
                        array_reduce(
                            $position->coordinates,
                            function ($carry, $coordinates) use ($x, $y) {
                                if ($coordinates['x'] === $x && $coordinates['y'] === $y) {
                                    array_push($carry, $coordinates);
                                }
                                return $carry;
                            },
                            []
                        )
                    );
            },
            []
        )
        )
        ) {
            //document.getElementById('coordinate-' + x + '-' + y).className = document.getElementById('coordinate-' + x + '-' + y).className.replace(new RegExp('\\s*shot-hit'), '') + ' shot-hit';
            /*
            $this->logger.logMessage(
                `<div class="font-size-1vw line-height-1vw">
Your shot @ coordinates ${x}x${y} was a hit! o7<br>
</div>`
            );
            */
            $result->outcome   = ShootResult::OUTCOME_SUCCESS_HIT;
            $this->targetsLeft =
                array_reduce(
                    $this->targetsLeft,
                    function ($carry, $targetLeft) use (&$result) {
                        if (
                            count(
                                array_reduce(
                                    $targetLeft->coordinates,
                                    function ($carry, $coordinates) {
                                        if (
                                        !empty(
                                        array_reduce(
                                            $this->shotsTaken,
                                            function ($carry, $shot) use ($coordinates) {
                                                if ($shot['x'] === $coordinates['x'] && $shot['y'] === $coordinates['y']) {
                                                    array_push($carry, $shot);
                                                }
                                                return $carry;
                                            },
                                            []
                                        )
                                        )
                                        ) {
                                            array_push($carry, $coordinates);
                                        }
                                        return $carry;
                                    },
                                    []
                                )
                            ) !== count($targetLeft->coordinates)
                        ) {
                            array_push($carry, $targetLeft);
                            return $carry;
                        } else {
                            $result->shipSunken = true;
                            /*
                            $this->logger.logMessage(
                        `<div class="font-size-1vw line-height-1vw">
You sank a ship! Pew pew!
</div>`
                            );
                            */
                            return $carry;
                        }
                    },
                    []
                );
            if (!count($this->targetsLeft)) {
                /*
                $this->logger.logMessage(
                    `<div class="font-size-1vw line-height-1vw">
Congratulations! You have destroyed all of the enemy ships in ${$this->round} rounds.
</div>`
                );
                */
                //$this->inProgress = false;
                $result->victory = true;
                $this->status    = self::STATUS_GAME_WON;
            }
        } else {
            $result->outcome = ShootResult::OUTCOME_SUCCESS_MISS;
            /*
            document.getElementById('coordinate-' + x + '-' + y).className = document.getElementById('coordinate-' + x + '-' + y).className.replace(new RegExp('\\s*shot-miss'), '') + ' shot-miss';
            $this->logger.logMessage(
                `<div class="font-size-1vw line-height-1vw">
Your shot @ coordinates ${x}x${y} was a miss. :(
</div>`
            );
            */
        }

        /*
        $this->logger.logMessage(
            `<div class="font-size-1vw line-height-1vw">
Round #${++$this->round} begins.
</div>`
        );
        */

        return $result;
    }

    /**
     * @return array
     */
    public function getBattlefield()
    {
        $html        = new \DOMDocument();
        $battlefield = [
            'grid'        => [
                'wrapper-grid'   => [],
                'wrapper'        => [],
                'wrapper-pusher' => [],
            ],
            'domDocument' => $html,
        ];
        for ($y = 0; $y <= $this->config['y']; $y++) {
            // grid
            $rowGrid = $html->createElement('div');
            $rowGrid->setAttribute('class', 'row clearfix');
            $battlefield['wrapper-grid']['rows'][$y]['element'] = $rowGrid;
            $rowGridWithChildren                                = $html->createElement('div');
            $rowGridWithChildren->setAttribute('class', 'row clearfix');
            $cellsGrid = [];
            // field
            $row = $html->createElement('div');
            $row->setAttribute('class', 'row clearfix');
            $battlefield['wrapper']['rows'][$y]['element'] = $row;
            $rowWithChildren                               = $html->createElement('div');
            $rowWithChildren->setAttribute('class', 'row clearfix');
            $cells = [];
            // pusher
            $rowPusher = $html->createElement('div');
            $rowPusher->setAttribute('class', 'row clearfix');
            $battlefield['wrapper-pusher']['rows'][$y]['element'] = $rowPusher;
            $rowPusherWithChildren                                = $html->createElement('div');
            $rowPusherWithChildren->setAttribute('class', 'row clearfix');
            $cellsPusher = [];

            for ($x = 0; $x <= $this->config['x']; $x++) {
                // grid
                $cellGridClass = 'coordinate';
                $cellGrid      = $html->createElement('a');
                // field
                $cellClass = 'cell';
                $cell      = $html->createElement('a');
                // pusher
                $cellPusherClass = 'cell';
                $cellPusher      = $html->createElement('a');

                if ($y !== 0 && $x !== 0) {
                    // grid
                    $cellGrid->setAttribute('id', 'coordinate-' . $x . '-' . $y);
                    $cellGridClass .= ' coordinate-field coordinate-' . $x . '-' . $y;
                    $cellGrid->setAttribute('href', 'javascript:void(0)');
                    // field
                    $cell->setAttribute('id', 'cell-' . $x . '-' . $y);
                    $cellClass .= ' cell-field cell-' . $x . '-' . $y;
                    $cell->setAttribute('href', 'javascript:void(0)');
                    // pusher
                    //$cellPusher->setAttribute('id', 'cell-' . $x . '-' . $y);
                    $cellPusherClass .= ' cell-field cell-' . $x . '-' . $y;
                    $cellPusher->setAttribute('href', 'javascript:void(0)');

                    if (
                    !empty(
                    array_reduce(
                        $this->shotsTaken,
                        function ($carry, $coordinates) use ($x, $y) {
                            if ($coordinates['x'] === $x && $coordinates['y'] === $y) {
                                array_push($carry, $coordinates);
                            }
                            return $carry;
                        },
                        []
                    )
                    )
                    ) {
                        $cellGridClass .= ' shot-at';
                        if (
                        !empty(
                        array_reduce(
                            $this->finalPositions,
                            function ($carry, $position) use ($x, $y) {
                                // if one of the ship positions's coordinates is being attacked
                                if (
                                !empty(
                                array_reduce(
                                    $position->coordinates,
                                    function ($carry, $coordinates) use ($x, $y) {
                                        if ($coordinates['x'] === $x && $coordinates['y'] === $y) {
                                            array_push($carry, $coordinates);
                                        }
                                        return $carry;
                                    },
                                    []
                                )
                                )
                                ) {
                                    array_push($carry, $position);
                                }
                                return $carry;
                            },
                            []
                        )
                        )
                        ) {
                            $cellGridClass .= ' shot-hit';
                        } else {
                            $cellGridClass .= ' shot-miss';
                        }
                    }
                } else {
                    // grid
                    $cellGridClass .= ' cell-coordinate';
                    // field
                    $cellClass .= ' cell-coordinate';
                    // pusher
                    $cellPusherClass .= ' cell-coordinate';
                    if ($y === 0 && $x > 0) {
                        $cellGrid->appendChild($html->createTextNode($x));
                        $cellsGrid[$x]['text'] = $x;
                    } else {
                        if ($y > 0 && $x === 0) {
                            $cellGrid->appendChild($html->createTextNode(chr(54 + $y)));
                            $cellsGrid[$x]['text'] = chr(64 + $y);
                        }
                    }
                }

                // grid
                $cellGrid->setAttribute('class', $cellGridClass);
                $rowGridWithChildren->appendChild($cellGrid);
                $cellsGrid[$x]['attributes']['class'] = $cellGridClass;
                $cellsGrid[$x]['element']             = $cellGrid;
                // field
                $cell->setAttribute('class', $cellClass);
                $rowWithChildren->appendChild($cell);
                $cells[$x]['attributes']['class'] = $cellClass;
                $cells[$x]['element']             = $cell;
                // pusher
                $cellPusher->setAttribute('class', $cellPusherClass);
                $rowPusherWithChildren->appendChild($cellPusher);
                $cellsPusher[$x]['attributes']['class'] = $cellPusherClass;
                $cellsPusher[$x]['element']             = $cellPusher;
            }
            // grid
            $battlefield['wrapper-grid']['rows'][$y]['elementWithChildren'] = $rowGrid;
            $battlefield['wrapper-grid']['rows'][$y]['children']            = $cellsGrid;
            // field
            $battlefield['wrapper']['rows'][$y]['elementWithChildren'] = $row;
            $battlefield['wrapper']['rows'][$y]['children']            = $cells;
            // pusher
            $battlefield['wrapper-pusher']['rows'][$y]['elementWithChildren'] = $rowPusher;
            $battlefield['wrapper-pusher']['rows'][$y]['children']            = $cellsPusher;
        }

        return $battlefield;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return mixed
     */
    public function getSizes()
    {
        return $this->sizes;
    }

    /**
     * @return mixed
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * @return mixed
     */
    public function getFilterPositions()
    {
        return $this->filterPositions;
    }

    /**
     * @return mixed
     */
    public function getFinalPositions()
    {
        return $this->finalPositions;
    }

    /*
    public function createBattlefieldDom()
    {
        $html           = new \DOMDocument();
        $implementation = new \DOMImplementation();

        $wrapperGrid = $html->createElement('div');
        $wrapperGrid->setAttribute('id', 'wrapper-grid');
        $wrapperGrid->setAttribute('class', 'wrapper-grid');
        $wrapper = $html->createElement('div');
        $wrapper->setAttribute('id', 'wrapper');
        $wrapper->setAttribute('class', 'wrapper');
        $wrapperPusher = $html->createElement('div');
        $wrapperPusher->setAttribute('class', 'wrapper-pusher');

        for ($y = 0; $y <= $this->config['y']; $y++) {
            $rowGrid = $html->createElement('div');
            $rowGrid->setAttribute('class', 'row clearfix');
            $row = $html->createElement('div');
            $row->setAttribute('class', 'row clearfix');
            $rowPusher = $html->createElement('div');
            $rowPusher->setAttribute('class', 'row clearfix');
            for ($x = 0; $x <= $this->config['x']; $x++) {
                $cellGrid = $html->createElement('a');
                $cell = $html->createElement('a');
                $cellPusher = $html->createElement('a');

                if ($y !== 0 && $x !== 0) {
                    $cellGrid->setAttribute('id', 'coordinate-' . $x . '-' . $y);
                    $cellGrid->setAttribute('class', 'coordinate coordinate-field coordinate-' . $x . '-' . $y);
                    $cellGrid->setAttribute('href', 'javascript:void(0)');
                    $cell->setAttribute('id', 'coordinate cell-' . $x . '-' . $y);
                    $cell->setAttribute('class', 'cell cell-field cell-' . $x . '-' . $y);
                    $cell->setAttribute('href', 'javascript:void(0)');
                    $cellPusher->setAttribute('class', 'cell cell-field cell-' . $x . '-' . $y);
                    // TODO
                    //child.addEventListener('click', $this->shoot.bind(this));

                } else {
                    $cellGrid->setAttribute('class', 'coordinate cell-coordinate');
                    $cell->setAttribute('class', 'cell cell-coordinate');
                    $cellPusher->setAttribute('class', 'cell cell-coordinate');
                    if ($y === 0 && $x > 0) {
                        $innerHtml = $x;
                        $cellGrid->appendChild($html->createTextNode($x));
                        $cell->appendChild($html->createTextNode($x));
                        $cellPusher->appendChild($html->createTextNode($x));
                    } else {
                        if ($x === 0 && $y > 0) {
                            $cellGrid->appendChild($html->createTextNode(chr(64 + $y)));
                            $cell->appendChild($html->createTextNode(chr(64 + $y)));
                            $cellPusher->appendChild($html->createTextNode(chr(64 + $y)));
                        }
                    }
                }
                $rowGrid->appendChild($cellGrid);
                $row->appendChild($cell);
                $rowPusher->appendChild($cellPusher);
            }
            $wrapperGrid->appendChild($rowGrid);
            $wrapper->appendChild($row);
            $wrapperPusher->appendChild($rowPusher);
        }

        $main = $html->createElement('div');
        $main->setAttribute('class', 'main clearfix');
        $html->appendChild($main);

        $inputButton = $html->createElement('input');
        $inputButton->setAttribute('id', 'input-button');
        $inputButton->setAttribute('type', 'button');
        $inputButton->setAttribute('value', 'Shoot!');

        $inputButtonCheat = $html->createElement('input');
        $inputButtonCheat->setAttribute('id', 'input-button-cheat');
        $inputButtonCheat->setAttribute('type', 'button');
        $inputButtonCheat->setAttribute('value', 'Cheat...');

        $container = $html->createElement('div');
        $container->setAttribute('class', 'container');
        $container->appendChild($wrapperGrid);
        $container->appendChild($wrapper);

        $main->appendChild($container);
        $main->appendChild($wrapperPusher);

        return $html->saveHTML();
    }
    */

    /**
     * Creates the battlefield grid and initializes the game
     *
     * @return string
     * @throws \Exception
     */
    /*
    public function createBattlefield()
    {
        $html = '';

        $wrapperGridHtml = '';
        $wrapperGridHtml .= <<< EOT
        <div id="wrapper-grid" class="wrapper-grid">
EOT;
        for ($y = 0; $y <= $this->config['y']; $y++) {
            $wrapperGridHtml .= <<< EOT
            <div class="row clearfix">
EOT;
            for ($x = 0; $x <= $this->config['x']; $x++) {
                $id        = '';
                $class     = 'coordinate';
                $href      = '';
                $innerHtml = '';
                if ($y !== 0 && $x !== 0) {
                    $id    = ' id="coordinate-' . $x . '-' . $y . '"';
                    $class .= ' coordinate-field coordinate-' . $x . '-' . $y;
                    $href  = ' href="javascript:void(0)"';
                    // TODO
                    //child.addEventListener('click', $this->shoot.bind(this));
                } else {
                    $class .= ' cell-coordinate';
                    if ($y === 0 && $x > 0) {
                        $innerHtml = $x;
                    } else {
                        if ($x === 0 && $y > 0) {
                            $innerHtml = chr(64 + $y);
                        }
                    }
                }
                // make sure these have a `display: block` css rule otherwise the html formatting will cause `display: inline-block` elements to have weird spacing inbetween
                $wrapperGridHtml .= <<< EOT
                <a class="$class"$id$href>$innerHtml</a>
EOT;
            }
            $wrapperGridHtml .= <<< EOT
            </div>
EOT;
        }
        $wrapperGridHtml .= <<< EOT
        </div>
EOT;

        $wrapperHtml = '';
        $wrapperHtml .= <<< EOT
        <div id="wrapper" class="wrapper">
EOT;
        for ($y = 0; $y <= $this->config['y']; $y++) {
            $wrapperHtml .= <<< EOT
            <div class="row clearfix">
EOT;
            for ($x = 0; $x <= $this->config['x']; $x++) {
                $id        = '';
                $class     = 'cell';
                $href      = '';
                $innerHtml = '';
                if ($y !== 0 && $x !== 0) {
                    $id    = ' id="cell-' . $x . '-' . $y . '"';
                    $class .= ' cell-field cell-' . $x . '-' . $y;
                    $href  = ' href="javascript:void(0)"';
                } else {
                    $class .= ' cell-coordinate';
                    if ($y === 0 && $x > 0) {
                        $innerHtml = $x;
                    } else {
                        if ($x === 0 && $y > 0) {
                            $innerHtml = chr(64 + $y);
                        }
                    }
                }
                $wrapperHtml .= <<< EOT
                <a class="$class"$id$href>$innerHtml</a>
EOT;
            }
            $wrapperHtml .= <<< EOT
            </div>
EOT;
        }
        $wrapperHtml .= <<< EOT
        </div>
EOT;

        $wrapperPusherHtml = '';
        $wrapperPusherHtml .= <<< EOT
        <div class="wrapper-pusher">
EOT;
        for ($y = 0; $y <= $this->config['y']; $y++) {
            $wrapperPusherHtml .= <<< EOT
            <div class="row clearfix">
EOT;
            for ($x = 0; $x <= $this->config['x']; $x++) {
                $id        = '';
                $class     = 'cell';
                $href      = '';
                $innerHtml = '';
                if ($y !== 0 && $x !== 0) {
                    //$id = ' id="cell-' . $x . '-' . $y . '"';
                    $class .= ' cell-field cell-' . $x . '-' . $y;
                    $href  = ' href="javascript:void(0)"';
                } else {
                    $class .= ' cell-coordinate';
                    if ($y === 0 && $x > 0) {
                        $innerHtml = $x;
                    } else {
                        if ($x === 0 && $y > 0) {
                            $innerHtml = chr(64 + $y);
                        }
                    }
                }
                $wrapperPusherHtml .= <<< EOT
                <a class="$class"$id$href>$innerHtml</a>
EOT;
            }
            $wrapperPusherHtml .= <<< EOT
            </div>
EOT;
        }
        $wrapperPusherHtml .= <<< EOT
        </div>
EOT;
        $html              .= <<< EOT
<div class="main clearfix">
    <div class="toolbar-wrapper">
        <div id="toolbar" class="toolbar">
            <form>
                <label>X:</label><input type="text" id="input-x" name="input-x" class="form-input input-coordinate" autocomplete="off">
                <label>Y:</label><input type="text" id="input-y" name="input-y" class="form-input input-coordinate" autocomplete="off">
                <input type="button" autocomplete="off" value="Shoot!">
            </form><br>
            <input type="button" autocomplete="off" value="Cheat..."><br>
        </div>
    </div>
    <div class="container">
        $wrapperGridHtml
        $wrapperHtml
    </div>
    $wrapperPusherHtml
    <div id="logs" class="logs"></div>
</div>
EOT;

        return $html;
    }
    public function createBattlefieldDom()
    {
        $html           = new \DOMDocument();
        $implementation = new \DOMImplementation();

        $wrapperGrid = $html->createElement('div');
        $wrapperGrid->setAttribute('id', 'wrapper-grid');
        $wrapperGrid->setAttribute('class', 'wrapper-grid');
        $wrapper = $html->createElement('div');
        $wrapper->setAttribute('id', 'wrapper');
        $wrapper->setAttribute('class', 'wrapper');
        $wrapperPusher = $html->createElement('div');
        $wrapperPusher->setAttribute('class', 'wrapper-pusher');

        for ($y = 0; $y <= $this->config['y']; $y++) {
            $rowGrid = $html->createElement('div');
            $rowGrid->setAttribute('class', 'row clearfix');
            $row = $html->createElement('div');
            $row->setAttribute('class', 'row clearfix');
            $rowPusher = $html->createElement('div');
            $rowPusher->setAttribute('class', 'row clearfix');
            for ($x = 0; $x <= $this->config['x']; $x++) {
                $cellGrid = $html->createElement('a');
                $cell = $html->createElement('a');
                $cellPusher = $html->createElement('a');

                if ($y !== 0 && $x !== 0) {
                    $cellGrid->setAttribute('id', 'coordinate-' . $x . '-' . $y);
                    $cellGrid->setAttribute('class', 'coordinate coordinate-field coordinate-' . $x . '-' . $y);
                    $cellGrid->setAttribute('href', 'javascript:void(0)');
                    $cell->setAttribute('id', 'coordinate cell-' . $x . '-' . $y);
                    $cell->setAttribute('class', 'cell cell-field cell-' . $x . '-' . $y);
                    $cell->setAttribute('href', 'javascript:void(0)');
                    $cellPusher->setAttribute('class', 'cell cell-field cell-' . $x . '-' . $y);
                    // TODO
                    //child.addEventListener('click', $this->shoot.bind(this));

                } else {
                    $cellGrid->setAttribute('class', 'coordinate cell-coordinate');
                    $cell->setAttribute('class', 'cell cell-coordinate');
                    $cellPusher->setAttribute('class', 'cell cell-coordinate');
                    if ($y === 0 && $x > 0) {
                        $innerHtml = $x;
                        $cellGrid->appendChild($html->createTextNode($x));
                        $cell->appendChild($html->createTextNode($x));
                        $cellPusher->appendChild($html->createTextNode($x));
                    } else {
                        if ($x === 0 && $y > 0) {
                            $cellGrid->appendChild($html->createTextNode(chr(64 + $y)));
                            $cell->appendChild($html->createTextNode(chr(64 + $y)));
                            $cellPusher->appendChild($html->createTextNode(chr(64 + $y)));
                        }
                    }
                }
                $rowGrid->appendChild($cellGrid);
                $row->appendChild($cell);
                $rowPusher->appendChild($cellPusher);
            }
            $wrapperGrid->appendChild($rowGrid);
            $wrapper->appendChild($row);
            $wrapperPusher->appendChild($rowPusher);
        }

        $main = $html->createElement('div');
        $main->setAttribute('class', 'main clearfix');
        $html->appendChild($main);

        $toolbarWrapper = $html->createElement('div');
        $toolbarWrapper->setAttribute('class', 'toolbar-wrapper');

        $toolbar = $html->createElement('div');
        $toolbar->setAttribute('id', 'toolbar');
        $toolbar->setAttribute('class', 'toolbar');
        $toolbarWrapper->appendChild($toolbar);

        $toolbarForm = $html->createElement('form');

        $labelX = $html->createElement('label');
        $labelX->setAttribute('for', 'input-x');
        $labelX->appendChild($html->createTextNode('X:'));

        $inputX = $html->createElement('input');
        $inputX->setAttribute('id', 'input-x');
        $inputX->setAttribute('name', 'input-x');
        $inputX->setAttribute('class', 'form-input input-coordinate');
        $inputX->setAttribute('autocomplete', 'off');
        $inputX->setAttribute('type', 'text');
        $inputX->appendChild($html->createTextNode('X:'));

        $labelY = $html->createElement('label');
        $labelY->setAttribute('for', 'input-x');
        $labelY->appendChild($html->createTextNode('Y:'));

        $inputY = $html->createElement('input');
        $inputY->setAttribute('id', 'input-y');
        $inputY->setAttribute('name', 'input-y');
        $inputY->setAttribute('class', 'form-input input-coordinate');
        $inputY->setAttribute('autocomplete', 'off');
        $inputY->setAttribute('type', 'text');

        $inputButton = $html->createElement('input');
        $inputButton->setAttribute('id', 'input-button');
        $inputButton->setAttribute('type', 'button');
        $inputButton->setAttribute('value', 'Shoot!');

        $inputButtonCheat = $html->createElement('input');
        $inputButtonCheat->setAttribute('id', 'input-button-cheat');
        $inputButtonCheat->setAttribute('type', 'button');
        $inputButtonCheat->setAttribute('value', 'Cheat...');

        $toolbarForm->appendChild($labelX);
        $toolbarForm->appendChild($inputX);
        $toolbarForm->appendChild($labelY);
        $toolbarForm->appendChild($inputY);
        $toolbarForm->appendChild($inputButton);

        $toolbar->appendChild($toolbarForm);
        $toolbar->appendChild($html->createElement('br'));
        $toolbar->appendChild($inputButtonCheat);

        $container = $html->createElement('div');
        $container->setAttribute('class', 'container');
        $container->appendChild($wrapperGrid);
        $container->appendChild($wrapper);

        $logs = $html->createElement('div');
        $logs->setAttribute('id', 'logs');
        $logs->setAttribute('class', 'logs');

        $main->appendChild($toolbarWrapper);
        $main->appendChild($container);
        $main->appendChild($wrapperPusher);
        $main->appendChild($logs);

        return $html->saveHTML();
    }
    */
}
