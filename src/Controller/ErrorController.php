<?php

namespace Smtm\Battleships\Controller;

use Smtm\Frameless\View\View;

/**
 * Class ErrorController
 * @package Smtm\Battleships\Controller
 */
class ErrorController
{
    /**
     * @var array
     */
    protected $config;
    /**
     * @var array
     */
    protected $request;
    /**
     * @var View
     */
    protected $view;

    /**
     * ErrorController constructor.
     *
     * @param array $config
     * @param $request
     * @param View $view
     */
    public function __construct(array $config, $request, View $view)
    {
        $this->config  = $config;
        $this->request = $request;
        $this->view    = $view;
    }

    /**
     * @return View
     */
    public function notFound(): View
    {
        http_response_code(404);
        return $this->view->setContentTemplate('404');
    }
}
