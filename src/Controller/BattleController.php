<?php

namespace Smtm\Battleships\Controller;

use Smtm\Battleships\Model\Battle;
use Smtm\Frameless\View\JsonView;

/**
 * Class BattleController
 *
 * @package Smtm\Battleships\Controller
 */
class BattleController
{
    /**
     * @var array
     */
    protected $config;
    /**
     * @var array
     */
    protected $request;
    /**
     * @var JsonView
     */
    protected $view;
    /**
     * @var Battle|null
     */
    protected $battle;

    /**
     * BattleController constructor.
     * @param array $config
     * @param $request
     * @param JsonView $view
     * @param Battle|null $battle
     */
    public function __construct(array $config, $request, JsonView $view, ?Battle $battle = null)
    {
        $this->config  = $config;
        $this->request = $request;
        $this->view    = $view;
        $this->battle  = $battle;
    }

    /**
     * @return JsonView
     */
    public function read(): JsonView
    {
        return
            $this
                ->view
                ->setVariables(
                    [
                        'data' => [
                            'battle' => $this->battle,
                        ],
                    ]
                );
    }

    /**
     * @return JsonView
     */
    public function shoot(): JsonView
    {
        $x = isset($this->request['request']['x']) ? intval($this->request['request']['x']) : null;
        $y = isset($this->request['request']['y']) ? intval($this->request['request']['y']) : null;

        $result = null;
        if ($this->battle !== null) {
            $result = $this->battle->shoot($x, $y);
        }

        $this->view->setVariables(
            [
                'input' => [
                    'x' => $x,
                    'y' => $y,
                ],
                'data'  => [
                    'result' => $result,
                ],
            ]
        );

        return $this->view;
    }

    /**
     * @return JsonView
     */
    public function cheat(): JsonView
    {
        $result = null;
        if ($this->battle !== null) {
            $result = $this->battle->getFinalPositions();
        }

        return
            $this
                ->view
                ->setVariables(
                    [
                        'data' => [
                            'finalPositions' => $result,
                        ],
                    ]
                );
    }
}
