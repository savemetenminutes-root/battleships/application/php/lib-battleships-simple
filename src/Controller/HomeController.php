<?php

namespace Smtm\Battleships\Controller;

use Smtm\Battleships\Model\Battle;
use Smtm\Frameless\View\View;

/**
 * Class HomeController
 * @package Smtm\Battleships\Controller
 */
class HomeController
{
    /**
     * @var array
     */
    protected $config;
    /**
     * @var array
     */
    protected $request;
    /**
     * @var View
     */
    protected $view;
    /**
     * @var Battle|null
     */
    protected $battle;

    /**
     * HomeController constructor.
     *
     * @param array $config
     * @param $request
     * @param View $view
     * @param Battle|null $battle
     */
    public function __construct(array $config, $request, View $view, ?Battle $battle = null)
    {
        $this->config  = $config;
        $this->request = $request;
        $this->view    = $view;
        $this->battle  = $battle;
    }

    /**
     * @return View
     */
    public function home(): View
    {
        $headTemplate    = 'battlefield-pre-head';
        $contentTemplate = 'battlefield-pre';
        if ($this->battle instanceof Battle) {
            $headTemplate    = 'battlefield-in-progress-head';
            $contentTemplate = 'battlefield-in-progress';
            $this->view->setVariables(
                [
                    'battlefield' => $this->battle->getBattlefield(),
                ]
            );
        }

        $this
            ->view
            ->setLayout($this->config['templates']['layout'])
            ->setHeadTemplate($headTemplate)
            ->setContentTemplate($contentTemplate);

        return $this->view;
    }
}
