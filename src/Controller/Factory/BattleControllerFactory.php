<?php

namespace Smtm\Battleships\Controller\Factory;

use Smtm\Battleships\Controller\BattleController;
use Smtm\Battleships\Model\Battle;
use Smtm\Frameless\View\JsonView;

/**
 * Class BattleControllerFactory
 *
 * @package Smtm\Battleships\Controller\Factory
 */
class BattleControllerFactory
{
    /**
     * @param array $config
     * @param $request
     * @return BattleController
     */
    public function __invoke(array $config, $request)
    {
        $viewFactory = $config['dependencies'][JsonView::class];
        $viewFactory = new $viewFactory();
        $view        = $viewFactory($config, $request);

        $battleFactory = $config['dependencies'][Battle::class];
        $battleFactory = new $battleFactory();
        $battle        = $battleFactory($config, $request);

        return new BattleController($config, $request, $view, $battle);
    }
}
