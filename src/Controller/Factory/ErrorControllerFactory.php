<?php

namespace Smtm\Battleships\Controller\Factory;

use Smtm\Battleships\Controller\ErrorController;
use Smtm\Frameless\View\View;

/**
 * Class ErrorControllerFactory
 *
 * @package Smtm\Battleships\Controller\Factory
 */
class ErrorControllerFactory
{
    /**
     * @param array $config
     * @param $request
     * @return ErrorController
     */
    public function __invoke(array $config, $request)
    {
        $viewFactory = $config['dependencies'][View::class];
        $viewFactory = new $viewFactory();
        $view        = $viewFactory($config, $request);
        return new ErrorController($config, $request, $view);
    }
}
