<?php

namespace Smtm\Battleships\Controller\Factory;

use Smtm\Battleships\Controller\HomeController;
use Smtm\Battleships\Model\Battle;
use Smtm\Frameless\View\View;

/**
 * Class HomeControllerFactory
 *
 * @package Smtm\Battleships\Controller\Factory
 */
class HomeControllerFactory
{
    /**
     * @param array $config
     * @param $request
     * @return HomeController
     */
    public function __invoke(array $config, $request)
    {
        $viewFactory = $config['dependencies'][View::class];
        $viewFactory = new $viewFactory();
        $view        = $viewFactory($config, $request);

        $battleFactory = $config['dependencies'][Battle::class];
        $battleFactory = new $battleFactory();
        $battle        = $battleFactory($config, $request);

        return new HomeController($config, $request, $view, $battle);
    }
}
